<?php
$servername = "db";
$username = "root";
$password = "D111111d";
$dbname = "db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT f.firstname, l.lastname FROM firstname f JOIN lastname l ON f.id = l.id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo $row["firstname"] . " " . $row["lastname"] . " from " . $_ENV["HOSTNAME"] . "<br>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>